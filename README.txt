v0.1 openwrt_8021x_gui

http://wiki.openwrt.org/doc/devel/luci
http://luci.subsignal.org/trac/wiki/Documentation/ModulesHowTo

2 Möglichkeiten: 
A: wan port irgendwo anstecken wos ohne auth internet gibt
	opkg update
	opkg remove wpad-mini
	opkg install wpad

B: wpad pkg für die entsprechende openwrtversion runterladen und per scp drauf laden
	opkg remove wpad-mini
	opkg install <PATHTOPACKAGE>

tar xzvf openwrt_8021x_gui.tar.gz -C /
/etc/init.d/wpa enable

configure webinterface -> top tabs: Wired Auth -> 802.1X (if it does not appear, reboot)
interface= <put here your wan interface (e.g. eth1)>

reboot after save+apply

If you got feedback of any kind please open a issue or merge request or drop me a mail: jake42 a.t rommelwood.de
