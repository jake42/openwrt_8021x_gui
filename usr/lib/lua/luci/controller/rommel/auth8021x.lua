--[[
	GUI for wpa_supplicant 8021x wired configuration
]]

module("luci.controller.rommel.auth8021x", package.seeall)

function index()
	entry({"admin", "auth8021x"}, firstchild(), "Wired Auth", 30).dependent=false
	entry({"admin", "auth8021x", "auth"}, cbi("rommel/wpa"), "802.1X", 1)
end
