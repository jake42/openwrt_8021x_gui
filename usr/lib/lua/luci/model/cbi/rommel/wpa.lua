m = Map("wpa_supplicant", "wpa", "Edits the config from which the wpa.conf is generated")

s = m:section(TypedSection, "wired", "Wired config for wpa-supplicant") 

interface = s:option(Value, "interface", translate("Physical inferface to be used"))

eaptype = s:option(ListValue, "eap_type", translate("EAP-Method"))                        
eaptype:value("TTLS", "TTLS")
eaptype:value("PEAP", "PEAP")     

auth = s:option(ListValue, "auth", translate("Authentication"))  
auth:value("auth=PAP", "PAP")                                                                                             
auth:value("auth=MSCHAPV2", "MSCHAPV2")     

cacert = s:option(FileUpload, "ca_cert", translate("Path to CA-Certificate"))

identity = s:option(Value, "identity", translate("Identity"))
password = s:option(Value, "password", translate("Password"))
password.password=true
return m
